from django.urls import path, include
from .views import GetAnimals


urlpatterns = [
    path("animals/", GetAnimals.as_view()),
    path("animals/<int:animal_id>/", GetAnimals.as_view())
]
