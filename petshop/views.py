from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response 
from petshop.serializers import AnimalSerializer, GroupSerializer, CharacteristicSerializer
from petshop.models import Animal, Group, Characteristic
from rest_framework import status

class GetAnimals(APIView):
    def get(self, request, animal_id=''):
        if animal_id:
            animal = get_object_or_404(Animal, id=animal_id)
            serialized = AnimalSerializer(animal)
            return Response(serialized.data)

        animal = Animal.objects.all()
        serialized = AnimalSerializer(animal, many=True)
        return Response(serialized.data)

    def post(self, request):
        serializer = AnimalSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        validated_data = serializer.validated_data
        
        characteristics = validated_data.pop('characteristics')
        groups = validated_data.pop('group')
        print(groups)
        group = Group.objects.get_or_create(**groups)
        print(group)

        serializer.validated_data["group"] = group[0]
        animal = Animal.objects.create(**serializer.validated_data)
        
        for char in characteristics:
            characteristic = Characteristic.objects.get_or_create(**char)[0]
            characteristic.animal.add(animal)

        serializer = AnimalSerializer(animal)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def delete(self, request, animal_id):
        animal = get_object_or_404(Animal, id=animal_id)
        
        animal.delete()
        
        return Response(status=status.HTTP_204_NO_CONTENT)

 



