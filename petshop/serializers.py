from rest_framework import serializers
from collections import OrderedDict


class GroupSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=False)
    scientific_name = serializers.CharField(required=False)
    
    
class CharacteristicSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)    
    name = serializers.CharField(required=False)

class AnimalSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(required=False)
    age = serializers.FloatField(required=False)
    weight = serializers.FloatField(required=False)
    sex = serializers.CharField(required=False)
    group = GroupSerializer()
    characteristics = CharacteristicSerializer(many=True)

    
    
